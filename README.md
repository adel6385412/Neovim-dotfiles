# Neovim-dotfiles

This is my personal neovim setup which includes keymaps, plugins, options and colorscheme.
Using this setup should give you a working neovim config straight out of box. (Provided you have the prerequisites)

## Note for Windows Users

Some parts of this config are not guarenteed to work for windows and in general the instructions do not include windows instructions. I do plan to add them _as soon as I figure out how to have a completely working config on Windows_ Until then, I don't recommend trying it on windows simply because it's a massive headache to deal with. Stick to macos or Linux, the setup is identical.

## Dependencies

- Neovim latest version 0.8 or later - https://github.com/neovim/neovim/wiki/installing-Neovim

  I recommend building this from source as I've personally had the best and most stable results this way.

- Ripgrep - for faster fuzzy finding - https://github.com/BurntSushi/ripgrep#installation
- A true color terminal - iTerm2, Alacritty, Kitty... etc
- A Nerd font - https://www.nerdfonts.com/font-downloads

  My personal favorite is CaskadiaCove

- Git - Obviously
- nodejs - Needed by Mason to install some of the LSP clients https://nodejs.org/en/download/package-manager
- npm - needed by node and should be auto installed with node

## Getting started

1. Clone this repo - `git clone https://gitlab.com/adel6385412/Neovim-dotfiles.git`
2. Navigate into Neovim-dotfiles `cd Neovim-dotfiles`
3. Run `chmod +x setup.sh` - This makes the included setup.sh script executable. It will automatically create the needed directories and copy the contents of this repo into it.
4. Run `./setup.sh`
5. `nvim ~/.config/nvim/lua/user/plugins-setup.lua` - You might see errors here and things might look very strange. This is normal. Ignore them and allow packer to install all the plugins. If it doesn't do it automatically then simply `:w` to save the file and packer should start installing.
6. Wait for Packer to be done installing all the packages. This shouldn't take too long.
7. Once Packer is done, quit nvim and navigate to the nvim config directory `cd ~/.config/nvim` followed by running `nvim` - You should now see it updated with a theme and get multiple notification about Mason installing LSP servers/linters/formatters...etc.
8. You're essentially done with the setup. You should now be able to enjoy this nvim config.

## helpful notes:

- `nvim plugins-setup.lua` - This is where all the plugins are installed using packer. Packer auto updated whenever this file is saved.
- You can find the `keymaps.lua`, `options.lua` and `colorscheme.lua` in `~/.config/nvim/lua/user/core` - Change anything there to your liking.
- Some plugins include their own keymapping in their dedicated lua file inside of `~/.config/nvim/lua/user/plugins/`
- For checking the docs of any of the plugins or commands run `:h name-of-plugin/command`
- For checking your installed LSP servers run `:Mason`
- Terminal is integrated and the keybinding for it is `alt-i`
- Default keybinding for toggling the file tree is `space-e` create file/dir: `a`, remove file/dir: `d`, rename:`r`
- You can run `:checkhealth` to see the state of your config and if you need to fix anything.

## Recommended tools alongside this config

- lazygit - An awesome git tool that's leagues better than just working with default git in the cli
  https://github.com/jesseduffield/lazygit#installation
- Using zsh with my oh-my-zsh and some nice theme will go a long way into making the integrated terminal look and feel nicer.

## Included Packages:

WIP - For now just refer to the comments inside of `~/.config/nvim/lua/user/plugins-setup.lua`

## TODO:

- List out all currently installed plugins and explain what they do
- Screenshots/gifs of the working setup
- Script to allow for a custom config process (provides choices for plugins, options, keybindings..etc interactively)
