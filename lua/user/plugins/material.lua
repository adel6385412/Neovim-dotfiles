local setup, material = pcall(require, "material")
if not setup then
	return
end

material.setup({
	lualine_style = "stealth",
})
