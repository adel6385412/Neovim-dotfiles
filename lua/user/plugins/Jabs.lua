local setup, jabs = pcall(require, "jabs")
if not setup then
	return
end

jabs.setup()
