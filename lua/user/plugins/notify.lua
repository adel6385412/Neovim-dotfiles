local setup, notify = pcall(require, "notify")
if not setup then
	return
end

notify.setup({
	options = {
		timeout = 2000,
		stages = "slide_in_slide_out",
	},
})
