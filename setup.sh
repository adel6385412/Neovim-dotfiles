#!/bin/bash

echo "Creating nvim directory in ~/.config..."
mkdir -p ~/.config/nvim

echo "Done."
echo "Copying repo contents into ~/.config/nvim..."

cp -r ./* ~/.config/nvim/

echo "Done."
echo "Removing setup.sh from nvim directory"

rm ~/.config/nvim/setup.sh
